<?php
class Owoifier{
	private $text;
	private $emotes = [
		'(・`ω´・)',
		';;w;;',
		'owo',
		'uwu',
		'>w<',
		'>_<',
		'^w^',
		'^_^',
		'>O<',
		'>u<',
	];
	
	function __construct(string $text){
		$this->text = $text;
	}
	
	function owo(){
		return new Owoifier(preg_replace(
		[
		'/([Ww])orse/m',
		'/[Tt]han/m', '/THAN/m',
		'/([Mm])ore/m', '/MORE/m',
		'/You|YOU/m', '/you/m',
		'/ove/m', '/OVE/m',
		'/n([aiueo])/m', '/N([aiueo])/m', '/N([AIUEO])/m',
		
		'/r|l/m', '/R|L/m',
		'/[Tt]h(?![Ee])/m', '/TH(?!E)/m',
		],
		[
		'\1ose',
		'den', 'DEN',
		'moaw', 'MOAW',
		'U', 'u',
		'uv', 'UV',
		'ny\1', 'Ny\1', 'NY\1',
		
		'w', 'W',
		'f', 'F',
		],
		$this->text
		));
	}
	
	function __toString(){
	# for print / echo
		return $this->text;
	}
	
	function appendEmote(){
		return new Owoifier(preg_replace_callback(
		'/$/m',
		function ($match){
			return ' ' . $this->emotes[array_rand($this->emotes)];
		},
		$this->text
		));
	}
	
	function prependEmote(){
		return new Owoifier(preg_replace_callback(
		'/^/m',
		function ($match){
			return $this->emotes[array_rand($this->emotes)] . ' ';
		},
		$this->text
		));
	}
}

function url(){ # https://stackoverflow.com/questions/2820723/how-do-i-get-the-base-url-with-php
  return sprintf(
    "%s://%s%s",
    isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https' : 'http',
    $_SERVER['SERVER_NAME'],
    $_SERVER['REQUEST_URI']
  );
}

$method = filter_input(INPUT_SERVER, 'REQUEST_METHOD', FILTER_SANITIZE_ENCODED);
$link = substr(filter_input(INPUT_SERVER, 'REQUEST_URI'), 1, -1);
$sn = htmlspecialchars(url());

if ($method == 'GET'){
	$get = filter_input_array(INPUT_GET);
	if (array_key_exists('msg', $get)){
	# process message
		$o = new Owoifier($get['msg']);
		$resp = [
			'str' => strval($o->owo()),
			'links' => [
				'actions' => [
					'append_emojis' => $link.'&action=append_emoji',
					'prepend_emojis' => $link.'&action=prepend_emoji',
				],
			],
		];
		
		if (array_key_exists('action', $get)){
			if($get['action'] == 'append_emoji')
				$resp['str'] = strval($o->owo()->appendEmote());
			else if ($get['action'] == 'prepend_emoji')
				$resp['str'] = strval($o->owo()->prependEmote());
		}
		
		# json response
		header('Content-Type: application/json');
		echo json_encode($resp);
	} else {
	# render home page
		echo("
<!DOCTYPE html>
<html>
	<head>
		<title>Owoifier API!!</title>
		<meta charset=\"utf-8\">
		<style>
			body{font-family: 'Comic Sans MS', 'Comic Sans', sans-serif; text-align: center;}
			pre{text-align: left; margin: auto; padding:0 30%; background:rgba(100,100,100,.5);}
			ul{text-align: left;margin: auto; padding:10px 30%;}
		</style>
	</head>
	<body>
		<h1>Owoifier API</h1>
		<p>OwO what's this?? *notices your owoifier* It'd be a shame if I...
		   tuwned it into a \"WESTful\" API?!?! OwO</p>
		<p>R u tiwed of having to have a bot.. or... an app... for your
		   owoifier needs?? O.O Neva feaw!! Owoifier API is heaw!!! ^w^</p>
		<p>Cuz u tech ppl love ur JSONs n ur REST n ur <a href=\"https://gitlab.com/zumid/owoifier-api\">open souwces</a>,,, we have it
		   all!</p>
		<p>dat sed...  Iz still vewy basic tho and it awways goes into max owodrive,,, sowwy *sob* ToT</p>
		<h2>Towtally WESTful? uwu</h2>
		<p>No it isn't .O.;;;;;</p>
		<p>It's just fow da owo... no need fow POST, PATCH, ow any oda verb!! aww
		   u need is GET .w.
		<p>So u can use it from ur web bowsah!! isn't it
		   convenient??? ^w^</p>
		<h2>Pwivacy?? .w.;;;;;</h2>
		<p>We dun need ur data!! ^w^ Iz safe!!!</p>
		<h2>HOW</h2>
		<p>Enter to ur web browsah (or cURL): <code>{$sn}?msg=Hello%20world!</code></p>
		<p>U should have dis as an ouput:<pre><code>
{
  \"str\": \"Hewwo wowwd!\",
  \"links\": {
    \"actions\": {
      \"append_emojis\": \"?msg=Hello%20world&action=append_emoji\",
      \"prepend_emojis\": \"?msg=Hello%20world&action=prepend_emoji\"
    }
  }
}

</code></pre></p>
		<h2>HWAT</h2>
		<p>Params:</p>
		<ul>
			<li><code>msg</code> - string to owoify</li>
			<li><code>action</code> - what to do after owoifying
				<ul>
					<li><code>append_emojis</code> - append an emoji after every line</li>
					<li><code>prepend_emojis</code> - prepend an emoji before every line</li>
				</ul>
			</li>
		</ul>
		<h2>Cwedits (・ω・)</h2>
		<ul>
			<li>LeafySweets - for making the original owoify chrome extension</li>
			<li><a href=\"https://github.com/mohan-cao\">mohan-cao</a> - for making the <a href=\"https://github.com/mohan-cao\">owoify-js</a> package that I referenced</li>
		</ul>
		<footer>
			made in boredom by <a href=\"https://gitlab.com/zumid\">zumi</a> 2020
		</footer>
	</body>
</html>
");
	}
} else if ($method == 'POST') {
	$post = filter_input_array(INPUT_POST);
	print_r($post);
}
?>